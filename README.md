# Simple segmentation network.

Implements a [UNet](https://towardsdatascience.com/medical-image-segmentation-part-1-unet-convolutional-networks-with-interactive-code-70f0f17f46c6) using vgg as the backbone.

To create/edit sample install the [labelme](https://github.com/wkentaro/labelme) tool with:

```
conda create --name=labelme python=3.6
source activate labelme
pip install pyqt5
pip install labelme
```

Now execute *labelme* with:
```
labelme
```
and open the Training or Validation folder of this repository.








