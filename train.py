#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 17:53:30 2018

@author: caiom
"""

import numpy as np
import matplotlib.pyplot as plt
import vgg_unet
import torch
from data_utils import SegmentationDataset
from torch.utils.data import DataLoader

gt_folder_train = '/home/caiom/Segmentation/Training'
gt_folder_val = '/home/caiom/Segmentation/Validation'
model_name = 'segm.pth'
patience = 10
plot_val = True
plot_train = True

max_epochs = 100
resolution = (608, 800)
class_weights = [1, 1, 1, 1]

# Color in RGB
class_to_color = {'Sky': (0, 0, 127), 'Ground': (0, 127, 127), 'Fruit': (0, 255, 0), 'Tree': (127, 0, 0)}
class_to_id = {'Sky': 0, 'Ground': 1, 'Fruit': 2, 'Tree': 3}
id_to_class = {v: k for k, v in class_to_id.items()}

train_dataset = SegmentationDataset(gt_folder_train, gt_folder_train, True, class_to_id, resolution, True)
train_loader = DataLoader(train_dataset, batch_size=1, shuffle=True, num_workers=2, drop_last=False)

val_dataset = SegmentationDataset(gt_folder_val, gt_folder_val, False, class_to_id, resolution)
val_loader = DataLoader(val_dataset, batch_size=1, shuffle=True, num_workers=2, drop_last=False)


if plot_train:

    for i_batch, sample_batched in enumerate(train_loader):
    
            image_np = np.squeeze(sample_batched['image_original'].cpu().numpy())
            gt = np.squeeze(sample_batched['gt'].cpu().numpy())
                
            color_label = np.zeros((resolution[1], resolution[0], 3))
            
            for key, val in id_to_class.items():
                color_label[gt == key] = class_to_color[val]
                
            plt.figure()
            plt.imshow((image_np/255) * 0.5 + (color_label/255) * 0.5)
            plt.show()
            
            plt.figure()
            plt.imshow(color_label.astype(np.uint8))
            plt.show()
    
model = vgg_unet.UNetVgg().cuda()

core_lr = 0.02
base_vgg_weight, base_vgg_bias, core_weight, core_bias = vgg_unet.UNetVgg.get_params_by_kind(model, 7)

optimizer = torch.optim.SGD([{'params': base_vgg_bias, 'lr': 0.000001}, 
                             {'params': base_vgg_weight, 'lr': 0.000001},
                             {'params': core_bias, 'lr': core_lr},
                             {'params': core_weight, 'lr': core_lr, 'weight_decay': 0.0005}], momentum=0.9)
    
lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 30, gamma=0.2) 

best_val_acc = -1
best_epoch = 0
# Start training...
for epoch in range(max_epochs):
    
    print('Epoch %d starting...' % (epoch+1))
    
    lr_scheduler.step()
    
    model.train()
    
    mean_loss = 0.0
    
    for i_batch, sample_batched in enumerate(train_loader):
    
    
        image = sample_batched['image'].cuda()
        gt = sample_batched['gt'].cuda()
    
        optimizer.zero_grad()
        total_loss = model.eval_net_with_loss(model, image, gt, class_weights)
        total_loss.backward()
        optimizer.step()
        
        mean_loss += total_loss.cpu().detach().numpy()
        
    mean_loss /= len(train_loader)
        
    print('Total loss: %f' % mean_loss)
    
    
    n_correct = 0
    n_false = 0
    
    
    for i_batch, sample_batched in enumerate(val_loader):
    
    
        image = sample_batched['image'].cuda()
        image_np = np.squeeze(sample_batched['image_original'].cpu().numpy())
        gt = np.squeeze(sample_batched['gt'].cpu().numpy())
        
    
        label_out = model(image)
        label_out = torch.nn.functional.softmax(label_out, dim = 1)
        label_out = label_out.cpu().detach().numpy()
        label_out = np.squeeze(label_out)
        
        labels = np.argmax(label_out, axis=0)
        
        if plot_val:
            
            color_label = np.zeros((resolution[1], resolution[0], 3))
            
            for key, val in id_to_class.items():
                color_label[labels == key] = class_to_color[val]
                
            plt.figure()
            plt.imshow((image_np/255) * 0.5 + (color_label/255) * 0.5)
            plt.show()
            
            plt.figure()
            plt.imshow(color_label.astype(np.uint8))
            plt.show()
        
        valid_mask = gt != -1
        curr_correct = np.sum(gt[valid_mask] == labels[valid_mask])
        curr_false = np.sum(valid_mask) - curr_correct
        n_correct += curr_correct
        n_false += curr_false
        
        
    total_acc = n_correct / (n_correct + n_false)
    
    if best_val_acc < total_acc:
        best_val_acc = total_acc
        if epoch > 7:
            torch.save(model.state_dict(), model_name)
            print('New best validation acc. Saving...')
        best_epoch = epoch

    if (epoch - best_epoch) > patience:
        break
    
    print('Curr acc: %f -- Best acc: %f -- epoch %d.' % (total_acc, best_val_acc, best_epoch))   
        
        
    
    
